package utils

import (
	"coverart/config"
	"coverart/internal/scheduler"
	"coverart/internal/server"
	"github.com/davecgh/go-spew/spew"
	"github.com/sirupsen/logrus"
	"sync"
)

var logger = logrus.WithField("logger", "utils")

const serverEndingError = `Error starting HTTP server" error="accept tcp [::]:8080: use of closed network connection`

// StartHTTPServer starts the HTTP server
func StartHTTPServer(configData *config.Config, debugMode bool, sched scheduler.Scheduler) {
	httpServer, err := server.NewHTTPServer(*configData, debugMode, sched.Shutdown)
	if err != nil {
		logrus.WithError(err).Fatal("Error starting httpServer")
	}
	if err := httpServer.StartHTTPServer(); err != nil && err.Error() != serverEndingError {
		logger.WithError(err).Error("Error starting HTTP httpServer")
	}
}

// LoadConfig load the config from the file
func LoadConfig(configFilePath string) *config.Config {
	configData, err := config.NewConfig(configFilePath)
	if err != nil || configData == nil {
		logger.WithFields(logrus.Fields{
			"Error":          err,
			"ConfigFilePath": configFilePath,
		}).Fatal("Can't load or parse conf file")
	}
	logger.Debugf("Config %v", spew.Sdump(configData))
	return configData
}

// StartScheduler start the background scheduler
func StartScheduler(configData *config.Config) (scheduler.Scheduler, *sync.WaitGroup) {
	sched, err := scheduler.NewScheduler(*configData)
	if err != nil {
		logger.WithError(err).Fatal("Error starting scheduler")
	}
	wg := &sync.WaitGroup{}
	wg.Add(1)
	go sched.Loop(wg)
	return sched, wg
}
