package gettitle

import (
	"coverart/config"
	"github.com/stretchr/testify/assert"
	"testing"
)

// Not applicable : firewall issue
//func TestGetIcecastTitle_GoodPort(t *testing.T) {
//	title, err := GetIcecastTitle(config.TitleRetrievalConfig{URL: "https://www.station-millenium.com:7000/status-json.xsl"})
//	if err != nil {
//		assert.IsType(t, types.NoTitleError{}, err)
//		assert.Nil(t, title)
//	} else {
//		assert.NotNil(t, title)
//		assert.NotEmpty(t, title.Artist)
//		assert.NotEmpty(t, title.Title)
//	}
//}

func TestGetIcecastTitle_ClosedPort(t *testing.T) {
	title, err := GetIcecastTitle(config.TitleRetrievalConfig{URL: "http://localhost:1234"})
	assert.Nil(t, title)
	assert.Error(t, err)
	assert.Contains(t, err.Error(), "connection refused")
}

func TestGetIcecastTitle_WrongJson(t *testing.T) {
	title, err := GetIcecastTitle(config.TitleRetrievalConfig{URL: "https://www.station-millenium.com/comments/feed/"})
	assert.Nil(t, title)
	assert.EqualError(t, err, "invalid character '<' looking for beginning of value")
}
