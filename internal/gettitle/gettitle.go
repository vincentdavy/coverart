package gettitle

import (
	"coverart/config"
	types2 "coverart/internal/types"
	"encoding/json"
	"github.com/sirupsen/logrus"
	"io/ioutil"
	"net/http"
	"strings"
	"time"
)

const timeoutDuration = 10
const artistTitleSeparator = " - "

type icecast struct {
	Icestats struct {
		Source struct {
			Title string
		}
	}
}

var logger = logrus.WithField("logger", "GetTitle")

// GetIcecastTitle get the current title from Icecast server
func GetIcecastTitle(config config.TitleRetrievalConfig) (*types2.Track, error) {
	//get the json
	netClient := &http.Client{
		Timeout: time.Second * timeoutDuration,
	}
	resp, err := netClient.Get(config.URL)
	if err != nil {
		return nil, err
	}
	defer func() {
		if err := resp.Body.Close(); err != nil {
			logger.WithError(err).Warn("Error closing Icecast connection")
		}
	}()

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	icecastData := &icecast{}
	err = json.Unmarshal(data, icecastData)
	if err != nil {
		return nil, err
	}

	splitTitle := strings.Split(icecastData.Icestats.Source.Title, artistTitleSeparator)
	if len(splitTitle) != 2 || len(splitTitle[0]) == 0 || len(splitTitle[1]) == 0 {
		return nil, types2.NoTitleError{}
	}

	return &types2.Track{
		Artist: splitTitle[0],
		Title:  splitTitle[1],
	}, nil
}
