package db

import (
	"coverart/internal/types"
	"encoding/json"
	"fmt"
	"github.com/influxdata/influxdb1-client/models"
	client "github.com/influxdata/influxdb1-client/v2"
	"github.com/sirupsen/logrus"
	"strconv"
	"time"
)

// Close close the InfluxDB connection
func (influxDB influxDB) Close() {
	logger.Debug("Close InfluxDB connection")
	if err := influxDB.client.Close(); err != nil {
		logger.WithError(err).Warn("Error closing InfluxDB connection")
	}
}

// AddTrack insert a track into InfluxDB
func (influxDB influxDB) AddTrack(track types.Track) error {
	logger.WithFields(logrus.Fields{
		"Artist": track.Artist,
		"Title":  track.Title,
	}).Debug("Saving track into InfluxDB")
	point, err := client.NewPoint(influxMeasurement, map[string]string{
		influxIsTitleTag: influxIsTrackTagTrue,
	}, map[string]interface{}{
		influxFieldArtist: track.Artist,
		influxFieldTitle:  track.Title,
		influxFieldImage:  track.Image,
	})
	if err != nil {
		return err
	}

	return influxDB.saveTrack(point)
}

// AddTrack insert a empty track into InfluxDB
func (influxDB influxDB) AddEmptyTrack() error {
	logger.Debug("Saving empty track into InfluxDB")
	point, err := client.NewPoint(influxMeasurement, map[string]string{
		influxIsTitleTag: influxIsTrackTagFalse,
	}, map[string]interface{}{
		influxFieldArtist: "",
		influxFieldTitle:  "",
		influxFieldImage:  "",
	})
	if err != nil {
		return err
	}

	return influxDB.saveTrack(point)
}

func (influxDB influxDB) saveTrack(point *client.Point) error {
	batchPoints, err := client.NewBatchPoints(client.BatchPointsConfig{
		Database:        influxDB.database,
		RetentionPolicy: influxDB.retentionPolicy,
	})
	if err != nil {
		return err
	}

	batchPoints.AddPoint(point)
	err = influxDB.client.Write(batchPoints)
	if err != nil {
		return err
	}

	return nil
}

// GetLastTrack get the last recorded track without image data
func (influxDB influxDB) GetLastTrack() (*types.Track, error) {
	query := client.NewQueryWithRP(lastTrackQuery, influxDB.database, influxDB.retentionPolicy, defaultPrecision)
	resp, err := influxDB.client.Query(query)
	if err != nil || resp.Error() != nil {
		return nil, err
	}

	if len(resp.Results) == 0 || len(resp.Results[0].Series) == 0 ||
		len(resp.Results[0].Series[0].Columns) != 5 || len(resp.Results[0].Series[0].Values) != 1 {
		logger.Warn("No last track found (artist & title search)")
		return nil, nil
	}

	trackTime, err := extractTrackTime(resp.Results[0].Series[0].Values[0])
	if err != nil {
		return nil, err
	}
	isTrack, err := strconv.ParseBool(resp.Results[0].Series[0].Values[0][1].(string))
	if err != nil {
		return nil, err
	}

	track := &types.Track{
		Time:    *trackTime,
		IsTrack: isTrack,
		Artist:  resp.Results[0].Series[0].Values[0][2].(string),
		Title:   resp.Results[0].Series[0].Values[0][3].(string),
		IsImage: len(resp.Results[0].Series[0].Values[0][4].(string)) > 0,
	}

	return track, nil
}

// GetLastTrack get the last recorded track without image data
func (influxDB influxDB) GetLast5Tracks() ([]*types.Track, error) {
	query := client.NewQueryWithRP(last5TracksQuery, influxDB.database, influxDB.retentionPolicy, defaultPrecision)
	resp, err := influxDB.client.Query(query)
	if err != nil || resp.Error() != nil {
		return nil, err
	}

	if len(resp.Results) == 0 || len(resp.Results[0].Series) == 0 ||
		len(resp.Results[0].Series[0].Columns) != 4 || len(resp.Results[0].Series[0].Values) != 5 {
		logger.Warn("No last 5 tracks found")
		return nil, nil
	}

	tracks, err := processTrackList(resp)
	if err != nil {
		return nil, err
	}

	return tracks, nil
}

func processTrackList(resp *client.Response) ([]*types.Track, error) {
	tracks := make([]*types.Track, len(resp.Results[0].Series[0].Values))
	for i, row := range resp.Results[0].Series[0].Values {
		trackTime, err := extractTrackTime(row)
		if err != nil {
			return nil, err
		}

		tracks[i] = &types.Track{
			Time:    *trackTime,
			IsTrack: true,
			Artist:  row[1].(string),
			Title:   row[2].(string),
			IsImage: len(row[3].(string)) > 0,
		}
	}
	return tracks, nil
}

func (influxDB influxDB) GetTrackHistory(timestamp time.Time) ([]*types.Track, error) {
	resp, err := influxDB.generateQueryWithTime(tracksByTimeIntervalQuery, timestamp.Format(time.RFC3339), timestamp.Format(time.RFC3339))
	if err != nil {
		return nil, err
	}

	if len(resp.Results) == 0 || len(resp.Results[0].Series) == 0 ||
		len(resp.Results[0].Series[0].Columns) != 4 {
		logger.Warn("No track history found")
		return nil, nil
	}

	tracks, err := processTrackList(resp)
	if err != nil {
		return nil, err
	}

	return tracks, nil
}

func (influxDB influxDB) generateQueryWithTime(queryToUse string, timestamps ...interface{}) (*client.Response, error) {
	queryString := fmt.Sprintf(queryToUse, timestamps...)
	query := client.NewQueryWithRP(queryString, influxDB.database, influxDB.retentionPolicy, defaultPrecision)
	resp, err := influxDB.client.Query(query)
	if err != nil || resp.Error() != nil {
		return nil, err
	}

	return resp, nil
}

func extractTrackTime(row []interface{}) (*time.Time, error) {
	trackTimeInt, err := row[0].(json.Number).Int64()
	if err != nil {
		return nil, err
	}
	trackTime, err := models.SafeCalcTime(trackTimeInt, defaultPrecision)
	if err != nil {
		return nil, err
	}
	return &trackTime, nil
}

func (influxDB influxDB) GetImageDataForSpecifiedTimestamp(imageTime time.Time) (string, error) {
	resp, err := influxDB.generateQueryWithTime(imageFromTimestampQuery, imageTime.Format(time.RFC3339))
	if err != nil {
		return "", err
	}

	if len(resp.Results) == 0 || len(resp.Results[0].Series) == 0 ||
		len(resp.Results[0].Series[0].Columns) != 2 || len(resp.Results[0].Series[0].Values) != 1 {
		logger.WithField("Time", imageTime).Warn("No image found")
		return "", nil
	}

	return resp.Results[0].Series[0].Values[0][1].(string), nil
}
