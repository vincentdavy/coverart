package db

import (
	"coverart/config"
	"coverart/internal/types"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func TestNewInfluxDB(t *testing.T) {
	influx, err := NewInfluxDB(config.InfluxDBConfig{
		URL:             "test://localhost",
		Database:        "millenium",
		RetentionPolicy: "autogen",
	})
	assert.EqualError(t, err, "Unsupported protocol scheme: test, your address must start with http:// or https://")
	assert.Nil(t, influx)
}

func TestInfluxDB_AddTrack_RetrieveTrackArtistTitle(t *testing.T) {
	influx, err := NewInfluxDB(config.InfluxDBConfig{
		URL:             "http://influxdb:8086",
		Database:        "millenium",
		RetentionPolicy: "autogen",
	})
	assert.NoError(t, err)
	defer influx.Close()

	sourceTrack := types.Track{
		Artist: "Kygo",
		Title:  "Higher love",
		Image:  "test",
	}
	err = influx.AddTrack(sourceTrack)
	assert.NoError(t, err)

	track, err := influx.GetLastTrack()
	assert.NoError(t, err)

	assert.NotNil(t, track)
	assert.True(t, track.IsTrack)
	assert.True(t, track.IsImage)
	assert.Equal(t, sourceTrack.Artist, track.Artist)
	assert.Equal(t, sourceTrack.Title, track.Title)

	image, err := influx.GetImageDataForSpecifiedTimestamp(track.Time.Add(-time.Second))
	assert.NoError(t, err)
	assert.NotEmpty(t, image)
}

func TestInfluxDB_AddTrackEmpty_RetrieveTrackArtistTitle(t *testing.T) {
	influx, err := NewInfluxDB(config.InfluxDBConfig{
		URL:             "http://influxdb:8086",
		Database:        "millenium",
		RetentionPolicy: "autogen",
	})
	assert.NoError(t, err)
	defer influx.Close()

	err = influx.AddEmptyTrack()
	assert.NoError(t, err)

	track, err := influx.GetLastTrack()
	assert.NoError(t, err)

	assert.NotNil(t, track)
	assert.False(t, track.IsTrack)
	assert.False(t, track.IsImage)
	assert.Empty(t, track.Artist)
	assert.Empty(t, track.Title)
}

func TestInfluxDB_GetLast5Tracks(t *testing.T) {
	influx, err := NewInfluxDB(config.InfluxDBConfig{
		URL:             "http://influxdb:8086",
		Database:        "millenium",
		RetentionPolicy: "autogen",
	})
	assert.NoError(t, err)
	defer influx.Close()

	sourceTrack := types.Track{
		Artist: "Kygo",
		Title:  "Higher love",
		Image:  "test",
	}

	for i := 0; i < 5; i++ {
		err = influx.AddTrack(sourceTrack)
		assert.NoError(t, err)
	}

	tracks, err := influx.GetLast5Tracks()
	assert.NoError(t, err)
	assert.Len(t, tracks, 5)
	for i := 0; i < 5; i++ {
		assert.NotNil(t, tracks[i])
		assert.True(t, tracks[i].IsTrack)
		assert.True(t, tracks[i].IsImage)
		assert.Equal(t, sourceTrack.Artist, tracks[i].Artist)
		assert.Equal(t, sourceTrack.Title, tracks[i].Title)
	}

	tracks, err = influx.GetTrackHistory(time.Now().Add(5 * time.Second))
	assert.NoError(t, err)
	assert.True(t, len(tracks) > 0)
	for _, track := range tracks {
		assert.NotNil(t, track)
		assert.True(t, track.IsTrack)
		assert.NotEmpty(t, track.Artist)
		assert.NotEmpty(t, track.Title)
	}
}
