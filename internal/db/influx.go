package db

import (
	"coverart/config"
	"coverart/internal/types"
	"github.com/influxdata/influxdb1-client/v2"
	"github.com/sirupsen/logrus"
	"time"
)

const (
	influxMeasurement     = "Tracks"
	influxIsTitleTag      = "IsTrack"
	influxIsTrackTagTrue  = "True"
	influxIsTrackTagFalse = "False"
	influxFieldArtist     = "Artist"
	influxFieldTitle      = "Title"
	influxFieldImage      = "Image"

	defaultPrecision          = "s"
	lastTrackQuery            = `SELECT IsTrack, Artist, Title, Image FROM "Tracks" ORDER BY time DESC LIMIT 1`
	last5TracksQuery          = `SELECT Artist, Title, Image FROM "Tracks" WHERE "IsTrack" = 'True' ORDER BY time DESC LIMIT 5 OFFSET 1`
	imageFromTimestampQuery   = `SELECT Image FROM "Tracks" WHERE time >= '%s' AND "IsTrack" = 'True' LIMIT 1`
	tracksByTimeIntervalQuery = `SELECT Artist, Title, Image FROM "Tracks" WHERE time <= '%s' AND time >= '%s' - 1h AND "IsTrack" = 'True' ORDER BY time DESC`
)

var logger = logrus.WithField("logger", "db")

type influxDB struct {
	client          client.Client
	database        string
	retentionPolicy string
}

// InfluxDB interface for InfluxDB client
type InfluxDB interface {
	Close()
	AddTrack(track types.Track) error
	AddEmptyTrack() error
	GetLastTrack() (*types.Track, error)
	GetImageDataForSpecifiedTimestamp(imageTime time.Time) (string, error)
	GetLast5Tracks() ([]*types.Track, error)
	GetTrackHistory(imageTime time.Time) ([]*types.Track, error)
}

// NewInfluxDB create a new InfluxDB client
func NewInfluxDB(config config.InfluxDBConfig) (InfluxDB, error) {
	influxClient, err := client.NewHTTPClient(client.HTTPConfig{
		Addr: config.URL,
	})
	if err != nil {
		return nil, err
	}

	return &influxDB{
		client:          influxClient,
		database:        config.Database,
		retentionPolicy: config.RetentionPolicy,
	}, nil
}
