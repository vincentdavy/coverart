package server

import (
	"coverart/internal/types"
	"errors"
	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/suite"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"
)

type mockInfluxdb struct {
	mock.Mock
}

func (m *mockInfluxdb) Close() {
	m.Called()
}

func (m *mockInfluxdb) AddTrack(track types.Track) error {
	return m.Called(track).Error(0)
}

func (m *mockInfluxdb) GetLastTrack() (*types.Track, error) {
	args := m.Called()
	if args.Error(1) != nil {
		return nil, args.Error(1)
	}
	return args.Get(0).(*types.Track), nil
}

func (m *mockInfluxdb) GetImageDataForSpecifiedTimestamp(imageTime time.Time) (string, error) {
	args := m.Called(imageTime)
	return args.String(0), args.Error(1)
}

func (m *mockInfluxdb) AddEmptyTrack() error {
	panic("implement me")
}

func (m *mockInfluxdb) GetLast5Tracks() ([]*types.Track, error) {
	args := m.Called()
	if args.Error(1) != nil {
		return nil, args.Error(1)
	}
	return args.Get(0).([]*types.Track), nil
}

func (m *mockInfluxdb) GetTrackHistory(timestamp time.Time) ([]*types.Track, error) {
	args := m.Called(timestamp)
	if args.Error(1) != nil {
		return nil, args.Error(1)
	}
	return args.Get(0).([]*types.Track), nil
}

type serverTestSuite struct {
	suite.Suite
	mockInfluxdb *mockInfluxdb
	server       *server
}

func TestServerTestSuite(t *testing.T) {
	suite.Run(t, new(serverTestSuite))
}

func (s *serverTestSuite) SetupTest() {
	s.mockInfluxdb = &mockInfluxdb{}
	s.server = &server{
		router:   gin.Default(),
		influxDB: s.mockInfluxdb,
	}
	s.server.setupServer()
}

func (s *serverTestSuite) TearDownTest() {
	s.mockInfluxdb.AssertExpectations(s.T())
}

func (s *serverTestSuite) TestGetLastTrack_GoodCase() {
	s.mockInfluxdb.On("GetLastTrack").Return(&types.Track{
		Time:   time.Now(),
		Artist: "Test",
		Title:  "Test",
	}, nil).Once()
	w := httptest.NewRecorder()
	req, err := http.NewRequest("GET", "/coverart/api/v1/getLastTrack", nil)
	s.Assert().NoError(err)
	s.server.router.ServeHTTP(w, req)

	s.Assert().Equal(200, w.Code)
	s.Assert().Contains(w.Body.String(), "Test")
	s.Assert().Equal("*", w.Header().Get(acaoHeader))
}

func (s *serverTestSuite) TestGetLastTrack_Error() {
	s.mockInfluxdb.On("GetLastTrack").Return(nil, errors.New("test")).Once()
	w := httptest.NewRecorder()
	req, err := http.NewRequest("GET", "/coverart/api/v1/getLastTrack", nil)
	s.Assert().NoError(err)
	s.server.router.ServeHTTP(w, req)

	s.Assert().Equal(500, w.Code)
	s.Assert().Equal(w.Body.String(), "An error occurred")
	s.Assert().Equal("*", w.Header().Get(acaoHeader))
}

func (s *serverTestSuite) TestGetLastTrackForRDS_GoodCase() {
	s.mockInfluxdb.On("GetLastTrack").Return(&types.Track{
		Time:    time.Now(),
		Artist:  "Test",
		Title:   "Test",
		IsTrack: true,
	}, nil).Once()
	w := httptest.NewRecorder()
	req, err := http.NewRequest("GET", "/coverart/api/v1/getLastTrack/rds", nil)
	s.Assert().NoError(err)
	s.server.router.ServeHTTP(w, req)

	s.Assert().Equal(200, w.Code)
	s.Assert().Contains(w.Body.String(), `<?xml version="1.0" encoding="UTF-8"?>
<currentSong available="true" xmlns="http://www.station-millenium.com/CurrentTitle"><artist>Test</artist><title>Test</title></currentSong>`)
	s.Assert().Equal("*", w.Header().Get(acaoHeader))
}

func (s *serverTestSuite) TestGetLastTrackForRDS_GoodCase_NoTitle() {
	s.mockInfluxdb.On("GetLastTrack").Return(&types.Track{
		Time:    time.Now(),
		IsTrack: false,
	}, nil).Once()
	w := httptest.NewRecorder()
	req, err := http.NewRequest("GET", "/coverart/api/v1/getLastTrack/rds", nil)
	s.Assert().NoError(err)
	s.server.router.ServeHTTP(w, req)

	s.Assert().Equal(200, w.Code)
	s.Assert().Contains(w.Body.String(), `<?xml version="1.0" encoding="UTF-8"?>
<currentSong available="false" xmlns="http://www.station-millenium.com/CurrentTitle"></currentSong>`)
	s.Assert().Equal("*", w.Header().Get(acaoHeader))
}

func (s *serverTestSuite) TestGetLastTrackForRDS_Error() {
	s.mockInfluxdb.On("GetLastTrack").Return(nil, errors.New("test")).Once()
	w := httptest.NewRecorder()
	req, err := http.NewRequest("GET", "/coverart/api/v1/getLastTrack/rds", nil)
	s.Assert().NoError(err)
	s.server.router.ServeHTTP(w, req)

	s.Assert().Equal(500, w.Code)
	s.Assert().Equal(w.Body.String(), "An error occurred")
	s.Assert().Equal("*", w.Header().Get(acaoHeader))
}

func (s *serverTestSuite) TestGetImageForTimestamp_Error() {
	w := httptest.NewRecorder()
	req, err := http.NewRequest("GET", "/coverart/api/v1/getImage/test", nil)
	s.Assert().NoError(err)
	s.server.router.ServeHTTP(w, req)

	s.Assert().Equal(400, w.Code)
	s.Assert().Equal(w.Body.String(), "Wrong time format")
	s.Assert().Equal("*", w.Header().Get(acaoHeader))
}

func (s *serverTestSuite) TestGetImageForTimestamp_InfluxError() {
	s.mockInfluxdb.On("GetImageDataForSpecifiedTimestamp", mock.AnythingOfType("time.Time")).Return("", errors.New("test")).Once()

	w := httptest.NewRecorder()
	req, err := http.NewRequest("GET", "/coverart/api/v1/getImage/"+time.Now().Format(time.RFC3339), nil)
	s.Assert().NoError(err)
	s.server.router.ServeHTTP(w, req)

	s.Assert().Equal(500, w.Code)
	s.Assert().Equal(w.Body.String(), "An error occurred")
	s.Assert().Equal("*", w.Header().Get(acaoHeader))
}

func (s *serverTestSuite) TestGetImageForTimestamp_NoData() {
	s.mockInfluxdb.On("GetImageDataForSpecifiedTimestamp", mock.AnythingOfType("time.Time")).Return("", nil).Once()

	w := httptest.NewRecorder()
	req, err := http.NewRequest("GET", "/coverart/api/v1/getImage/"+time.Now().Format(time.RFC3339), nil)
	s.Assert().NoError(err)
	s.server.router.ServeHTTP(w, req)

	s.Assert().Equal(404, w.Code)
	s.Assert().Equal(w.Body.String(), "Image not found")
	s.Assert().Equal("*", w.Header().Get(acaoHeader))
}

func (s *serverTestSuite) TestGetImageForTimestamp_Base64Error() {
	s.mockInfluxdb.On("GetImageDataForSpecifiedTimestamp", mock.AnythingOfType("time.Time")).Return("test test", nil).Once()

	w := httptest.NewRecorder()
	req, err := http.NewRequest("GET", "/coverart/api/v1/getImage/"+time.Now().Format(time.RFC3339), nil)
	s.Assert().NoError(err)
	s.server.router.ServeHTTP(w, req)

	s.Assert().Equal(500, w.Code)
	s.Assert().Equal(w.Body.String(), "An error occurred")
	s.Assert().Equal("*", w.Header().Get(acaoHeader))
}

func (s *serverTestSuite) TestGetImageForTimestamp_GoodCase() {
	s.mockInfluxdb.On("GetImageDataForSpecifiedTimestamp", mock.AnythingOfType("time.Time")).Return("test", nil).Once()

	w := httptest.NewRecorder()
	req, err := http.NewRequest("GET", "/coverart/api/v1/getImage/"+time.Now().Format(time.RFC3339), nil)
	s.Assert().NoError(err)
	s.server.router.ServeHTTP(w, req)

	s.Assert().Equal(200, w.Code)
	s.Assert().Len(w.Body.String(), 3)
	s.Assert().Equal("*", w.Header().Get(acaoHeader))
}

func (s *serverTestSuite) TestGetLast5Tracks_GoodCase() {
	s.mockInfluxdb.On("GetLast5Tracks").Return([]*types.Track{
		{
			Time:   time.Now(),
			Artist: "Test",
			Title:  "Test",
		},
		{
			Time:   time.Now(),
			Artist: "Test",
			Title:  "Test",
		},
		{
			Time:   time.Now(),
			Artist: "Test",
			Title:  "Test",
		},
		{
			Time:   time.Now(),
			Artist: "Test",
			Title:  "Test",
		},
		{
			Time:   time.Now(),
			Artist: "Test",
			Title:  "Test",
		},
	}, nil).Once()
	w := httptest.NewRecorder()
	req, err := http.NewRequest("GET", "/coverart/api/v1/getLast5Tracks", nil)
	s.Assert().NoError(err)
	s.server.router.ServeHTTP(w, req)

	s.Assert().Equal(200, w.Code)
	s.Assert().Contains(w.Body.String(), "Test")
	s.Assert().Equal("*", w.Header().Get(acaoHeader))
}

func (s *serverTestSuite) TestGetLast5Tracks_Error() {
	s.mockInfluxdb.On("GetLast5Tracks").Return(nil, errors.New("test")).Once()
	w := httptest.NewRecorder()
	req, err := http.NewRequest("GET", "/coverart/api/v1/getLast5Tracks", nil)
	s.Assert().NoError(err)
	s.server.router.ServeHTTP(w, req)

	s.Assert().Equal(500, w.Code)
	s.Assert().Equal(w.Body.String(), "An error occurred")
	s.Assert().Equal("*", w.Header().Get(acaoHeader))
}

func (s *serverTestSuite) TestGetTrackHistoryWithTime_Error() {
	w := httptest.NewRecorder()
	req, err := http.NewRequest("GET", "/coverart/api/v1/getTrackHistory/test", nil)
	s.Assert().NoError(err)
	s.server.router.ServeHTTP(w, req)

	s.Assert().Equal(400, w.Code)
	s.Assert().Equal(w.Body.String(), "Wrong time format")
	s.Assert().Equal("*", w.Header().Get(acaoHeader))
}

func (s *serverTestSuite) TestGetTrackHistoryWithTime_InfluxError() {
	s.mockInfluxdb.On("GetTrackHistory", mock.AnythingOfType("time.Time")).Return(nil, errors.New("test")).Once()

	w := httptest.NewRecorder()
	req, err := http.NewRequest("GET", "/coverart/api/v1/getTrackHistory/"+time.Now().Format(time.RFC3339), nil)
	s.Assert().NoError(err)
	s.server.router.ServeHTTP(w, req)

	s.Assert().Equal(500, w.Code)
	s.Assert().Equal(w.Body.String(), "An error occurred")
	s.Assert().Equal("*", w.Header().Get(acaoHeader))
}

func (s *serverTestSuite) TestGetTrackHistoryWithTime_GoodCase() {
	s.mockInfluxdb.On("GetTrackHistory", mock.AnythingOfType("time.Time")).Return(
		[]*types.Track{
			{
				Time:   time.Now(),
				Artist: "Test",
				Title:  "Test",
			},
			{
				Time:   time.Now(),
				Artist: "Test",
				Title:  "Test",
			},
		}, nil).Once()

	w := httptest.NewRecorder()
	req, err := http.NewRequest("GET", "/coverart/api/v1/getTrackHistory/"+time.Now().Format(time.RFC3339), nil)
	s.Assert().NoError(err)
	s.server.router.ServeHTTP(w, req)

	s.Assert().Equal(200, w.Code)
	s.Assert().Contains(w.Body.String(), "Test")
	s.Assert().Equal("*", w.Header().Get(acaoHeader))
}

func (s *serverTestSuite) TestGetTrackHistory_InfluxError() {
	s.mockInfluxdb.On("GetTrackHistory", mock.AnythingOfType("time.Time")).Return(nil, errors.New("test")).Once()

	w := httptest.NewRecorder()
	req, err := http.NewRequest("GET", "/coverart/api/v1/getTrackHistory", nil)
	s.Assert().NoError(err)
	s.server.router.ServeHTTP(w, req)

	s.Assert().Equal(500, w.Code)
	s.Assert().Equal(w.Body.String(), "An error occurred")
	s.Assert().Equal("*", w.Header().Get(acaoHeader))
}

func (s *serverTestSuite) TestGetTrackHistory_GoodCase() {
	s.mockInfluxdb.On("GetTrackHistory", mock.AnythingOfType("time.Time")).Return(
		[]*types.Track{
			{
				Time:   time.Now(),
				Artist: "Test",
				Title:  "Test",
			},
			{
				Time:   time.Now(),
				Artist: "Test",
				Title:  "Test",
			},
		}, nil).Once()

	w := httptest.NewRecorder()
	req, err := http.NewRequest("GET", "/coverart/api/v1/getTrackHistory", nil)
	s.Assert().NoError(err)
	s.server.router.ServeHTTP(w, req)

	s.Assert().Equal(200, w.Code)
	s.Assert().Contains(w.Body.String(), "Test")
	s.Assert().Equal("*", w.Header().Get(acaoHeader))
}
