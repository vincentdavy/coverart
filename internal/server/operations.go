package server

import (
	"coverart/internal/types"
	"encoding/base64"
	"encoding/xml"
	"github.com/gin-gonic/gin"
	"net/http"
	"time"
)

const (
	xmlns      = "http://www.station-millenium.com/CurrentTitle"
	acaoHeader = "Access-Control-Allow-Origin"
)

func (server server) getLastTrack(c *gin.Context) {
	setCORS(c)
	track, err := server.influxDB.GetLastTrack()
	if err != nil {
		logger.WithError(err).Error("Error getting last track")
		abortWithError(c, err)
		return
	}

	c.JSON(http.StatusOK, track)
}

func (server server) getLast5Tracks(c *gin.Context) {
	setCORS(c)
	tracks, err := server.influxDB.GetLast5Tracks()
	if err != nil {
		logger.WithError(err).Error("Error getting last 5 tracks")
		abortWithError(c, err)
		return
	}

	c.JSON(http.StatusOK, tracks)
}

func (server server) getLastTrackForRDS(c *gin.Context) {
	setCORS(c)
	track, err := server.influxDB.GetLastTrack()
	if err != nil {
		logger.WithError(err).Error("Error getting last track")
		abortWithError(c, err)
		return
	}

	_, err = c.Writer.Write([]byte(xml.Header))
	if err != nil {
		logger.WithError(err).Error("Error outputting XML header")
		return
	}

	c.XML(http.StatusOK, &struct {
		types.Track
		XMLName struct{} `xml:"currentSong"`
		Xmlns   string   `xml:"xmlns,attr"`
	}{
		Xmlns: xmlns,
		Track: *track,
	})
}

func abortWithError(c *gin.Context, err error) {
	c.String(http.StatusInternalServerError, "An error occurred")
	_ = c.Error(err)
	c.Abort()
}

func (server server) getImageForTimestamp(c *gin.Context) {
	setCORS(c)
	trackTime, err := parseTime(c)
	if err != nil {
		return
	}

	imageString, err := server.influxDB.GetImageDataForSpecifiedTimestamp(*trackTime)
	if err != nil {
		c.String(http.StatusInternalServerError, "An error occurred")
		_ = c.Error(err)
		c.Abort()
		return
	}

	if len(imageString) == 0 {
		c.String(http.StatusNotFound, "Image not found")
		c.Abort()
		return
	}

	imageBytes, err := base64.StdEncoding.DecodeString(imageString)
	if err != nil {
		c.String(http.StatusInternalServerError, "An error occurred")
		_ = c.Error(err)
		c.Abort()
		return
	}

	c.Data(http.StatusOK, "image/jpeg", imageBytes)
}

func (server server) getTrackHistoryWithTime(c *gin.Context) {
	setCORS(c)
	trackTime, err := parseTime(c)
	if err != nil {
		return
	}

	server.processTrackHistory(c, *trackTime)
}

func (server server) getTrackHistory(c *gin.Context) {
	setCORS(c)
	server.processTrackHistory(c, time.Now())
}

func (server server) processTrackHistory(c *gin.Context, time time.Time) {
	tracks, err := server.influxDB.GetTrackHistory(time)
	if err != nil {
		logger.WithError(err).Error("Error getting track history")
		abortWithError(c, err)
		return
	}

	c.JSON(http.StatusOK, tracks)
}

func parseTime(c *gin.Context) (*time.Time, error) {
	timeAsString := c.Param(timestampURI)
	trackTime, err := time.Parse(time.RFC3339, timeAsString)
	if err != nil {
		c.String(http.StatusBadRequest, "Wrong time format")
		_ = c.Error(err)
		c.Abort()
		return nil, err
	}
	return &trackTime, nil
}

func setCORS(c *gin.Context) {
	c.Writer.Header().Set(acaoHeader, "*")
}
