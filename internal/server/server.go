package server

import (
	"coverart/config"
	"coverart/internal/db"
	"github.com/fvbock/endless"
	"github.com/gin-contrib/expvar"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"syscall"
)

const (
	routePrefix  = "coverart"
	apiPrefix    = "/api"
	apiVersion   = "/v1"
	defaultPort  = ":8080"
	timestampURI = "timestamp"
)

var logger = logrus.WithField("logger", "server")

type server struct {
	influxDB     db.InfluxDB
	shutdownFunc func()
	router       *gin.Engine
}

// HTTPServer HTTP server interface
type HTTPServer interface {
	StartHTTPServer() error
}

// NewHTTPServer create a new HTTP server
func NewHTTPServer(config config.Config, debugMode bool, shutdownFunc func()) (HTTPServer, error) {
	influxDB, err := db.NewInfluxDB(config.InfluxDBConfig)
	if err != nil {
		return nil, err
	}

	if !debugMode {
		gin.SetMode(gin.ReleaseMode)
	}
	server := &server{
		shutdownFunc: shutdownFunc,
		router:       gin.Default(),
		influxDB:     influxDB,
	}

	return server, nil
}

// StartHTTPServer starts the HTTP port on specified port and binds all API URL
func (server server) setupServer() {
	server.router.GET(routePrefix+"/debug/vars", expvar.Handler())

	apiRouter := server.router.Group(routePrefix).Group(apiPrefix).Group(apiVersion)
	apiRouter.GET("/getLastTrack", server.getLastTrack)
	apiRouter.GET("/getLastTrack/rds", server.getLastTrackForRDS)
	apiRouter.GET("/getLast5Tracks", server.getLast5Tracks)
	apiRouter.GET("/getImage/:"+timestampURI, server.getImageForTimestamp)
	apiRouter.GET("/getTrackHistory", server.getTrackHistory)
	apiRouter.GET("/getTrackHistory/:"+timestampURI, server.getTrackHistoryWithTime)
}

func (server server) StartHTTPServer() error {
	server.setupServer()
	srv := endless.NewServer(defaultPort, server.router)
	srv.SignalHooks[endless.PRE_SIGNAL][syscall.SIGHUP] = append(srv.SignalHooks[endless.PRE_SIGNAL][syscall.SIGHUP], server.shutdownFunc)
	srv.SignalHooks[endless.PRE_SIGNAL][syscall.SIGUSR1] = append(srv.SignalHooks[endless.PRE_SIGNAL][syscall.SIGUSR1], server.shutdownFunc)
	srv.SignalHooks[endless.PRE_SIGNAL][syscall.SIGUSR2] = append(srv.SignalHooks[endless.PRE_SIGNAL][syscall.SIGUSR2], server.shutdownFunc)
	srv.SignalHooks[endless.PRE_SIGNAL][syscall.SIGINT] = append(srv.SignalHooks[endless.PRE_SIGNAL][syscall.SIGINT], server.shutdownFunc)
	srv.SignalHooks[endless.PRE_SIGNAL][syscall.SIGTERM] = append(srv.SignalHooks[endless.PRE_SIGNAL][syscall.SIGTERM], server.shutdownFunc)
	srv.SignalHooks[endless.PRE_SIGNAL][syscall.SIGTSTP] = append(srv.SignalHooks[endless.PRE_SIGNAL][syscall.SIGTSTP], server.shutdownFunc)
	logger.Info("Starting HTTP router")
	return srv.ListenAndServe()
}
