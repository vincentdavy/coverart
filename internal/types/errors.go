package types

// NoTitleError error when there is no data on Icecast
type NoTitleError struct {
	error
}
