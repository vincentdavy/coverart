package types

import "time"

// Track global track for the whole app
type Track struct {
	Time    time.Time `json:"time" xml:"-"`
	IsTrack bool      `json:"is_track" xml:"available,attr"`
	IsImage bool      `json:"is_image" xml:"-"`
	Artist  string    `json:"artist,omitempty" xml:"artist,omitempty"`
	Title   string    `json:"title,omitempty" xml:"title,omitempty"`
	Image   string    `json:"-" xml:"-"`
}
