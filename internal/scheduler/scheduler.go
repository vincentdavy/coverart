package scheduler

import (
	"coverart/config"
	"coverart/internal/db"
	"coverart/internal/images"
	"github.com/sirupsen/logrus"
	"sync"
)

var logger = logrus.WithField("logger", "scheduler")

type scheduler struct {
	config             config.Config
	influx             db.InfluxDB
	trackImageProvider images.TrackImageProvider
	exitChan           chan bool
}

// Scheduler interface to handle all access for all
type Scheduler interface {
	Shutdown()
	Loop(wg *sync.WaitGroup)
}

// NewScheduler create and initialize a new scheduler
func NewScheduler(config config.Config) (Scheduler, error) {
	influx, err := db.NewInfluxDB(config.InfluxDBConfig)
	if err != nil {
		return nil, err
	}

	trackImageProvider, err := images.NewTrackImageProvider(config.ImageProviderConfig)
	if err != nil {
		return nil, err
	}

	logger.Debug("Scheduler initialized")
	return &scheduler{
		config:             config,
		influx:             influx,
		trackImageProvider: trackImageProvider,
		exitChan:           make(chan bool),
	}, nil
}

// Shutdown shutdown the scheduler
func (sched *scheduler) Shutdown() {
	logger.Debug("Shutting down scheduler")
	sched.exitChan <- true
	close(sched.exitChan)
}
