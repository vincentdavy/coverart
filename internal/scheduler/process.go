package scheduler

import (
	"coverart/internal/gettitle"
	"coverart/internal/images"
	types2 "coverart/internal/types"
	"errors"
	"github.com/sirupsen/logrus"
)

func (sched scheduler) processTrack() {
	logger.Debug("Gathering title")
	track, err := gettitle.GetIcecastTitle(sched.config.TitleRetrievalConfig)
	if err != nil {
		if errors.Is(err, types2.NoTitleError{}) {
			sched.processEmptyTrack()
		} else {
			logger.WithError(err).Error("Error getting title on Icecast")
		}
		return
	}

	lastTrack, err := sched.influx.GetLastTrack()
	if err != nil {
		logger.WithError(err).Error("Error getting last track (artist & title)")
		return
	}

	if lastTrack == nil || !(track.Artist == lastTrack.Artist && track.Title == lastTrack.Title) {
		sched.processTrackImage(track)
		sched.saveTrack(track)
	} else {
		logger.Debug("Track already inserted")
	}
}

func (sched scheduler) processEmptyTrack() {
	lastTrack, err := sched.influx.GetLastTrack()
	if err != nil || lastTrack == nil {
		logger.WithError(err).Error("Error getting last track for empty track (artist & title)")
		return
	}
	if lastTrack.IsTrack {
		err = sched.influx.AddEmptyTrack()
		if err != nil {
			logger.WithError(err).Error("Error saving empty track into InfluxDB")
			return
		}
	}
	logger.Debug("No title currently available on Icecast")
}

func (sched scheduler) saveTrack(track *types2.Track) {
	err := sched.influx.AddTrack(*track)
	if err != nil {
		logger.WithField("Artist", track.Artist).WithField("Title", track.Title).WithError(err).Error("Error saving track into InfluxDB")
		return
	}
	logger.WithFields(logrus.Fields{
		"Artist": track.Artist,
		"Title":  track.Title,
		"Image":  len(track.Image) > 0,
	}).Info("Track saved")
}

func (sched scheduler) processTrackImage(track *types2.Track) {
	if imageBase64, found := sched.config.CustomImageList[types2.Track{
		Artist: track.Artist,
		Title:  track.Title,
	}]; found {
		logger.WithFields(logrus.Fields{
			"Artist": track.Artist,
			"Title":  track.Title,
		}).Debug("Use custom image")
		track.Image = imageBase64
	} else {
		trackImageURL, err := sched.trackImageProvider.GetImageURL(*track)
		if err != nil {
			logger.WithField("Artist", track.Artist).WithField("Title", track.Title).WithError(err).Error("Error getting track image URL")
			return
		}
		if trackImageURL != nil {
			imageBase64, err := images.DownloadImageAndConvertToBase64(trackImageURL)
			if err != nil {
				logger.WithField("Artist", track.Artist).WithField("Title", track.Title).WithError(err).Error("Error downloading track image")
				return
			}
			track.Image = imageBase64
		}
	}
}
