package scheduler

import (
	"sync"
	"time"
)

// Loop start and block into scheduler loop
func (sched scheduler) Loop(wg *sync.WaitGroup) {
	logger.WithField("Interval", sched.config.IntervalDuration).Info("Scheduler started")
	loop := true

	for loop {
		select {
		case <-sched.exitChan:
			loop = false
		case <-time.After(sched.config.IntervalDuration):
			sched.processTrack()
		}
	}

	sched.influx.Close()
	logger.Info("Scheduler stopped")
	wg.Done()
}
