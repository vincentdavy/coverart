package images

import (
	"coverart/config"
	"coverart/internal/types"
	"github.com/stretchr/testify/assert"
	"testing"
	"text/template"
)

const deezerURL = `https://api.deezer.com/search/track?q=artist:"{{.Artist}}"%20track:"{{.Title}}""`
const itunesURL = `https://itunes.apple.com/search?term={{.Artist}}%20{{.Title}}&limit=1&entity=song&media=music`

func TestNewDeezerProvider_WrongCase(t *testing.T) {
	conf := config.ImageProviderConfig{
		DeezerURL: "{{.Test}",
	}
	deezer, err := NewTrackImageProvider(conf)

	assert.Nil(t, deezer)
	assert.EqualError(t, err, "template: deezerURL:1: bad character U+007D '}'")
}

func TestNewDeezerProvider_GoodCase(t *testing.T) {
	conf := config.ImageProviderConfig{
		DeezerURL: "{{.Test}}",
	}
	deezer, err := NewTrackImageProvider(conf)

	assert.NotNil(t, deezer)
	assert.NoError(t, err)
}

func TestGetImageURLFromDeezer_GoodCase(t *testing.T) {
	conf := config.ImageProviderConfig{
		DeezerURL: deezerURL,
	}
	deezer, err := NewTrackImageProvider(conf)
	assert.NotNil(t, deezer)
	assert.NoError(t, err)

	imageURL, err := deezer.GetImageURL(types.Track{
		Artist: "Kygo",
		Title:  "Higher love",
	})

	assert.NoError(t, err)
	assert.NotNil(t, imageURL)
}

func TestGetImageURLFromDeezer_EmptyTitle(t *testing.T) {
	conf := config.ImageProviderConfig{
		DeezerURL: deezerURL,
	}
	deezer, err := NewTrackImageProvider(conf)
	assert.NotNil(t, deezer)
	assert.NoError(t, err)

	imageURL, err := deezer.GetImageURL(types.Track{})

	assert.Nil(t, imageURL)
	assert.Error(t, err)
}

func TestGetImageURLFromItunes_GoodCase(t *testing.T) {
	itunesURLTpl, err := template.New(itunesURLTpl).Parse(itunesURL)
	assert.NotNil(t, itunesURLTpl)
	assert.NoError(t, err)

	imageURL, err := processTrack(types.Track{
		Artist: "Kygo",
		Title:  "Higher love",
	}, itunesURLTpl, itunesJSONPath)

	assert.NoError(t, err)
	assert.NotNil(t, imageURL)
}
