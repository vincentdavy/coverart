package images

import (
	"encoding/base64"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
)

// DownloadImageAndConvertToBase64 download image and convert it to base64
func DownloadImageAndConvertToBase64(imageURL *url.URL) (string, error) {
	resp, err := http.Get(imageURL.String())
	if err != nil {
		return "", err
	}
	defer func() {
		if err := resp.Body.Close(); err != nil {
			logger.WithError(err).Warn("Error closing image download request")
		}
	}()
	if resp.StatusCode != http.StatusOK {
		return "", fmt.Errorf("wrong status code : %v", resp.StatusCode)
	}

	imageBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}

	imageBase64 := base64.StdEncoding.EncodeToString(imageBytes)
	logger.WithField("URL", imageURL).Debug("Download track image")
	return imageBase64, nil
}
