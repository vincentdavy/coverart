package images

import (
	"bytes"
	"coverart/config"
	"coverart/internal/types"
	"fmt"
	"github.com/sirupsen/logrus"
	"github.com/thedevsaddam/gojsonq"
	"io/ioutil"
	"net/http"
	"net/url"
	"text/template"
)

const (
	deezerURLTpl   = "deezerURL"
	itunesURLTpl   = "itunesURL"
	tmplArtistKey  = "Artist"
	tmplTitleKey   = "Title"
	deezerJSONPath = "data.[0].album.cover_medium"
	itunesJSONPath = "results.[0].artworkUrl100"
)

var logger = logrus.WithField("logger", "images")

type trackImageProvider struct {
	deezerURLTpl *template.Template
	itunesURLTpl *template.Template
}

// TrackImageProvider interface handling all templates for title providing
type TrackImageProvider interface {
	GetImageURL(track types.Track) (*url.URL, error)
}

// NewTrackImageProvider create a new track image provider
func NewTrackImageProvider(config config.ImageProviderConfig) (TrackImageProvider, error) {
	deezerURLTpl, err := template.New(deezerURLTpl).Parse(config.DeezerURL)
	if err != nil {
		return nil, err
	}
	itunesURLTpl, err := template.New(itunesURLTpl).Parse(config.ItunesURL)
	if err != nil {
		return nil, err
	}

	return &trackImageProvider{
		deezerURLTpl: deezerURLTpl,
		itunesURLTpl: itunesURLTpl,
	}, nil
}

// GetImageURL return the URL for an image
func (provider trackImageProvider) GetImageURL(track types.Track) (*url.URL, error) {
	logger.WithField("Artist", track.Artist).WithField("Title", track.Title).Debug("Query track image on Deezer")
	imageURL, err := processTrack(track, provider.deezerURLTpl, deezerJSONPath)
	if err != nil {
		return nil, err
	}

	if imageURL == nil {
		logger.WithField("Artist", track.Artist).WithField("Title", track.Title).Debug("Query track image on Itunes")
		imageURL, err = processTrack(track, provider.itunesURLTpl, itunesJSONPath)
		if err != nil {
			return nil, err
		}
	}

	return imageURL, nil
}

func processTrack(track types.Track, tpl *template.Template, jsonPath string) (*url.URL, error) {
	var buf bytes.Buffer
	err := tpl.Execute(&buf, map[string]string{
		tmplArtistKey: url.PathEscape(track.Artist),
		tmplTitleKey:  url.PathEscape(track.Title),
	})
	if err != nil {
		return nil, err
	}

	resp, err := http.Get(buf.String())
	if err != nil {
		return nil, err
	}
	defer func() {
		if err := resp.Body.Close(); err != nil {
			logger.WithError(err).Warn("Error closing request")
		}
	}()

	bodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	coverURLNullable := gojsonq.New().FromString(string(bodyBytes)).Find(jsonPath)
	if coverURLNullable == nil {
		logger.WithField("Artist", track.Artist).WithField("Title", track.Title).Debug("Image not found")
		return nil, nil
	}
	coverURLString, ok := coverURLNullable.(string)
	if !ok {
		return nil, fmt.Errorf("error converting %v to string", coverURLString)
	}

	coverURL, err := url.ParseRequestURI(coverURLString)
	if err != nil {
		return nil, err
	}

	return coverURL, nil
}
