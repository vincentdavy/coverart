package images

import (
	"github.com/stretchr/testify/assert"
	"net/url"
	"testing"
)

func TestImageURLToBase64Data_GoodCase(t *testing.T) {
	imageURL, err := url.Parse("https://e-cdns-images.dzcdn.net/images/cover/5f45ec14c6cc67e1428a60e3548be46c/250x250-000000-80-0-0.jpg")
	assert.NoError(t, err)
	assert.NotNil(t, imageURL)

	image, err := DownloadImageAndConvertToBase64(imageURL)

	assert.NoError(t, err)
	assert.Condition(t, func() (success bool) {
		return len(image) > 0
	})
}

func TestImageURLToBase64Data_Error(t *testing.T) {
	imageURL, err := url.Parse("https://e-cdns-images.dzcdn.net/images/cover/5f45ec14c6cc67e1428a60e3548be46c/fake-url.jpg")
	assert.NoError(t, err)
	assert.NotNil(t, imageURL)

	image, err := DownloadImageAndConvertToBase64(imageURL)

	assert.Error(t, err, "wrong status code : 404")
	assert.Len(t, image, 0)
}
