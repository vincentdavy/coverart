package main

import (
	"coverart/internal/utils"
	"expvar"
	"flag"
	"github.com/sirupsen/logrus"
	"os"
	"runtime"
)

const (
	configFileEnvVarName = "CONFIG_FILE"
	debugEnvVarName      = "DEBUG"
	expvarApp            = "appInfo"
	expvarVersion        = "version"
	expvarCommit         = "commit"
	expvarDate           = "date"
)

var version, commit, date = "undefined", "undefined", "undefined" //set at build time

var logger = logrus.WithField("logger", "main")

var (
	configFilePath string // flag
	debugMode      bool   //flag
)

func init() {
	flag.StringVar(&configFilePath, "configFile", "", "Config file path (or set via env var CONFIG_FILE)")
	flag.BoolVar(&debugMode, "debug", false, "Enable debug mode (or set env var DEBUG)")

	mapVar := expvar.NewMap(expvarApp)
	versionVar := &expvar.String{}
	versionVar.Set(version)
	mapVar.Set(expvarVersion, versionVar)

	commitVar := &expvar.String{}
	commitVar.Set(commit)
	mapVar.Set(expvarCommit, commitVar)

	dateVar := &expvar.String{}
	dateVar.Set(date)
	mapVar.Set(expvarDate, dateVar)
}

func main() {
	logger.WithFields(logrus.Fields{
		"Version": version,
		"Commit":  commit,
		"Date":    date,
		"Runtime": runtime.Version(),
	}).Info("Starting Coverart app")
	processFlags()

	configData := utils.LoadConfig(configFilePath)
	sched, wg := utils.StartScheduler(configData)

	utils.StartHTTPServer(configData, debugMode, sched)

	wg.Wait()
	logger.Info("Exiting")
}

func processFlags() {
	flag.Parse()

	if debugMode || len(os.Getenv(debugEnvVarName)) > 0 {
		logrus.SetLevel(logrus.DebugLevel)
		logger.Debug("Debug mode enabled")
	}

	if len(os.Getenv(configFileEnvVarName)) > 0 {
		configFilePath = os.Getenv(configFileEnvVarName)
	}

	if configFilePath == "" {
		flag.PrintDefaults()
		logger.Fatal("Missing parameters")
	}
}
