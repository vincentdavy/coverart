package config

import (
	"coverart/internal/types"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestNewConfig_NonExistingFile(t *testing.T) {
	conf, err := NewConfig("/toto")
	assert.Nil(t, conf)
	assert.EqualError(t, err, "open /toto: no such file or directory")
}

func TestNewConfig_WrongFile(t *testing.T) {
	conf, err := NewConfig("new.go")
	assert.Nil(t, conf)
	assert.EqualError(t, err, "yaml: line 72: mapping values are not allowed in this context")
}

func TestNewConfig_GoodCase(t *testing.T) {
	conf, err := NewConfig("../config.yml")
	assert.NoError(t, err)
	assert.NotNil(t, conf)
	assert.True(t, len(conf.CustomImageList) > 0)
	assert.Len(t, conf.CustomImageList[types.Track{
		Artist: "Flash",
		Title:  "Info",
	}], 11896)
}

func TestNewConfig_WrongValidation(t *testing.T) {
	conf, err := NewConfig("../config-wrong.yml")
	assert.Nil(t, conf)
	assert.EqualError(t, err, "invalid title retrieval URL : fake-url")
}
