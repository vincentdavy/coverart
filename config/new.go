package config

import (
	"coverart/internal/types"
	"encoding/base64"
	"github.com/sirupsen/logrus"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"os"
	"regexp"
)

const (
	customImageFilNameRegexp = `^([[:alnum:]]{1}([[:alnum:]]|[' éèàêùç])*[[:alnum:]]{1}) - ([[:alnum:]]{1}([[:alnum:]]|[' éèàêùç])*[[:alnum:]]{1})\.jpg$`
	subMatchCount            = 5
	artistSubMatchIndex      = 1
	titleSubMatchIndex       = 3
)

var fileNameMatcher *regexp.Regexp

func init() {
	fileNameMatcher = regexp.MustCompile(customImageFilNameRegexp)
}

// NewConfig load and parse the config file
func NewConfig(configFilePath string) (*Config, error) {
	configLogger.WithField("ConfigFile", configFilePath).Debug("Loading conf file")
	configFile, err := os.Open(configFilePath)
	if err != nil {
		return nil, err
	}

	config := &Config{}
	err = yaml.NewDecoder(configFile).Decode(&config)
	if err != nil {
		return nil, err
	}

	for _, config := range []validator{&config.TitleRetrievalConfig, config.ImageProviderConfig, config.InfluxDBConfig} {
		if err = config.validate(); err != nil {
			return nil, err
		}
	}

	err = config.loadCustomImages()
	if err != nil {
		return nil, err
	}

	return config, nil
}

func (c *Config) loadCustomImages() error {
	c.CustomImageList = make(map[types.Track]string)

	fis, err := ioutil.ReadDir(c.CustomImagePath)
	if err != nil {
		return err
	}

	for _, fi := range fis {
		if fileNameMatcher.MatchString(fi.Name()) && !fi.IsDir() {
			fileBytes, err := ioutil.ReadFile(c.CustomImagePath + "/" + fi.Name())
			if err != nil {
				return err
			}

			submatches := fileNameMatcher.FindStringSubmatch(fi.Name())
			if len(submatches) == subMatchCount {
				track := types.Track{
					Artist: submatches[artistSubMatchIndex],
					Title:  submatches[titleSubMatchIndex],
				}
				c.CustomImageList[track] = base64.StdEncoding.EncodeToString(fileBytes)
				configLogger.WithFields(logrus.Fields{
					"FileName": fi.Name(),
					"Artist":   submatches[artistSubMatchIndex],
					"Title":    submatches[titleSubMatchIndex],
				}).Debug("Added custom image file")
			}
		}
	}

	return nil
}
