package config

import (
	"errors"
	"fmt"
	"net/url"
	"os"
	"time"
)

func (c *TitleRetrievalConfig) validate() error {
	if _, err := url.ParseRequestURI(c.URL); err != nil {
		return fmt.Errorf("invalid title retrieval URL : %s", c.URL)
	}
	if len(c.Interval) == 0 {
		return errors.New("missing title retrieval interval")
	}
	var err error
	if c.IntervalDuration, err = time.ParseDuration(c.Interval); err != nil {
		return err
	}

	return nil
}

func (c ImageProviderConfig) validate() error {
	if len(c.DeezerURL) == 0 {
		return errors.New("missing Deezer URL")
	}

	if len(c.ItunesURL) == 0 {
		return errors.New("missing Itunes URL")
	}

	return nil
}

func (c InfluxDBConfig) validate() error {
	if _, err := url.ParseRequestURI(c.URL); err != nil {
		return fmt.Errorf("invalid Influx URL : %s", c.URL)
	}
	if len(c.Database) == 0 {
		return errors.New("missing Influx database")
	}
	if len(c.RetentionPolicy) == 0 {
		return fmt.Errorf("missing Influx retention policy")
	}

	return nil
}

func (c Config) validate() error {
	fi, err := os.Stat(c.CustomImagePath)
	if err != nil {
		return err
	}

	if !fi.IsDir() {
		return fmt.Errorf("custom image path %s is not a directory", c.CustomImagePath)
	}

	return nil
}
