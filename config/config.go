package config

import (
	"coverart/internal/types"
	"github.com/sirupsen/logrus"
	"time"
)

var configLogger = logrus.WithField("logger", "config")

type validator interface {
	validate() error
}

// Config hold the global config
type Config struct {
	TitleRetrievalConfig `yaml:"title-retrieval"`
	ImageProviderConfig  `yaml:"image-providers"`
	InfluxDBConfig       `yaml:"influxdb"`
	CustomImagePath      string `yaml:"custom-image-path"`
	CustomImageList      map[types.Track]string
}

// TitleRetrievalConfig config for title retrieval part
type TitleRetrievalConfig struct {
	URL              string `yaml:"url"`
	Interval         string `yaml:"interval"`
	IntervalDuration time.Duration
}

// ImageProviderConfig config for image providers and storage
type ImageProviderConfig struct {
	DeezerURL string `yaml:"deezer-url"`
	ItunesURL string `yaml:"itunes-url"`
}

// InfluxDBConfig config for influx db part
type InfluxDBConfig struct {
	URL             string `yaml:"url"`
	Database        string `yaml:"db"`
	RetentionPolicy string `yaml:"rp"`
}
